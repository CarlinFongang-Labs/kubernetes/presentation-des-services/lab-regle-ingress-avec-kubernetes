# Lab : Utilisation de Kubernetes Ingress pour Ouvrir l'Accès à un Service

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Créer un service pour exposer le déploiement d'authentification Web

2. Créer une entrée qui correspond au nouveau service

# Contexte

Votre entreprise, BeeHive, est en train de développer des applications pour Kubernetes.

L'une de ces applications, un service d'authentification Web, devra être accessible aux clients externes via l'application mobile BeeBox. Un déploiement a été créé pour représenter ce service (l'application en est encore à ses débuts, elle exécute donc simplement des conteneurs Nginx pour le moment).

Créez un service ClusterIP qui exposera le web-authdéploiement. Ensuite, créez une entrée qui mappe les demandes avec le chemin d'accès **`/auth`** au service.

Pour l’instant, vous n’avez pas à vous soucier de l’installation de contrôleurs Ingress.

>![Alt text](img/image.png)

# Application

## Introduction

Kubernetes Ingress vous permet de personnaliser la manière dont les entités externes peuvent interagir avec vos applications Kubernetes via le réseau. Ce laboratoire vous permettra d'exercer vos connaissances sur Kubernetes Ingress. Vous utiliserez Ingress pour ouvrir l'accès d'un service existant à un serveur externe.


## Étape 1 : Connexion au serveur de laboratoire

Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Créer un service pour exposer le déploiement d'authentification Web

1. Découvrons le déploiement `web-auth` :

```sh
kubectl get deployment web-auth -o yaml
   ```

>![Alt text](img/image-1.png)

   - Notons le label `app: web-auth` sur nos pods. Nous utiliserons ce label pour sélectionner ces pods à l'aide de notre service.

2. Créez un fichier `web-auth-svc.yml` :

```sh
nano web-auth-svc.yml
```

3. Collez les définitions YAML suivantes dans le fichier `web-auth-svc.yml` :

   **Remarque :** Pour coller du YAML directement dans l'éditeur Vi/Vim, entrez d'abord la commande `:set paste`.

```yaml
apiVersion: v1
kind: Service
metadata:
    name: web-auth-svc
spec:
  type: ClusterIP
  selector:
    app: web-auth
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: 80
```

4. Créez le service :

```sh
kubectl create -f web-auth-svc.yml
```

>![Alt text](img/image-2.png)
*Création du service ClusterIP réussi*

## Étape 3 : Créer une entrée qui correspond au nouveau service

1. Créez un fichier `web-auth-ingress.yml` :

```sh
nano web-auth-ingress.yml
```

2. Ajoutez les définitions YAML suivantes dans le fichier `web-auth-ingress.yml` :

   **Remarque :** Pour coller du YAML directement dans l'éditeur Vi/Vim, entrez d'abord la commande `:set paste`.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: web-auth-ingress
spec:
  rules:
  - http:
      paths:
      - path: /auth
        pathType: Prefix
        backend:
          service:
            name: web-auth-svc
            port:
              number: 80
```


3. Créez l'entrée Ingress :

```sh
kubectl create -f web-auth-ingress.yml
```

>![Alt text](img/image-3.png)
*Création de la règle ingress*

5. Vérifiez l'état de l'entrée Ingress :

```sh
kubectl describe ingress web-auth-ingress
```

>![Alt text](img/image-4.png)

   - Notez les points de terminaison du service dans la section `Backends` de la sortie. Vous avez créé avec succès une entrée Ingress qui correspond au service backend.

Ce laboratoire couvre la configuration d'un service Kubernetes et d'une entrée Ingress pour exposer un déploiement web d'authentification à des entités externes, vous permettant ainsi de mettre en pratique vos compétences en matière de mise en réseau Kubernetes.